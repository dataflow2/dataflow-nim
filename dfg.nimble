# Package
version       = "0.1.0"
author        = "Jan Adams"
description   = "dataflow programming using nim"
license       = "MIT"
srcDir        = "src"
bin           = @["dfg"]

# Dependencies
requires "jester >= 0.5.0"

task test, "Runs the test suite":
  exec "nim c -r tests/testsuite"