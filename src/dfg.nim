import jester

var db_connections = newSeq[string]()
var db_nodes = newSeq[string]()


routes:
  get "/":
    let html = readFile("src/index.html")
    resp html, "text/html"

  get "/connections/all":
    resp $db_connections

  post "/connections/@connection":
    db_connections.add(@"connection")
    resp $db_connections

  get "/nodes/all":
    resp $db_nodes


  post "/nodes/@node":
    db_nodes.add(@"node")
    resp $db_nodes
